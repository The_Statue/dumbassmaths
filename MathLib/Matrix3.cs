﻿using System;
namespace MathClasses
{
    public class Matrix3
    {
        public float m1, m2, m3, m4, m5, m6, m7, m8, m9;

        public Matrix3()
        {
            m1 = 1; m2 = 0; m3 = 0;
            m4 = 0; m5 = 1; m6 = 0;
            m7 = 0; m8 = 0; m9 = 1;
        }
        public Matrix3(float a, float b, float c, float d, float e, float f, float g, float h, float i)
        {
            m1 = a; m2 = b; m3 = c;
            m4 = d; m5 = e; m6 = f;
            m7 = g; m8 = h; m9 = i;
        }

        public static Matrix3 operator *(Matrix3 one, Matrix3 two)
        {
            Matrix3 sum = new Matrix3();

            sum.m1 = (one.m1 * two.m1) + (one.m4 * two.m2) + (one.m7 * two.m3);
            sum.m2 = (one.m2 * two.m1) + (one.m5 * two.m2) + (one.m8 * two.m3);
            sum.m3 = (one.m3 * two.m1) + (one.m6 * two.m2) + (one.m9 * two.m3);

            sum.m4 = (one.m1 * two.m4) + (one.m4 * two.m5) + (one.m7 * two.m6);
            sum.m5 = (one.m2 * two.m4) + (one.m5 * two.m5) + (one.m8 * two.m6);
            sum.m6 = (one.m3 * two.m4) + (one.m6 * two.m5) + (one.m9 * two.m6);

            sum.m7 = (one.m1 * two.m7) + (one.m4 * two.m8) + (one.m7 * two.m9);
            sum.m8 = (one.m2 * two.m7) + (one.m5 * two.m8) + (one.m8 * two.m9);
            sum.m9 = (one.m3 * two.m7) + (one.m6 * two.m8) + (one.m9 * two.m9);

            return sum;
        }

        public void SetRotateX(float angle)
        {
            m1 = 1; m2 = 0;                         m3 = 0;
            m4 = 0; m5 = (float)Math.Cos(angle);    m6 = (float)Math.Sin(angle);
            m7 = 0; m8 = -(float)Math.Sin(angle);   m9 = (float)Math.Cos(angle);
        }
        public void SetRotateY(float angle)
        {
            m1 = (float)Math.Cos(angle);    m2 = 0; m3 = -(float)Math.Sin(angle);
            m4 = 0;                         m5 = 1; m6 = 0;
            m7 = (float)Math.Sin(angle);    m8 = 0; m9 = (float)Math.Cos(angle);
        }
        public void SetRotateZ(float angle)
        {
            m1 = (float)Math.Cos(angle);    m2 = (float)Math.Sin(angle); m3 = 0;
            m4 = -(float)Math.Sin(angle);   m5 = (float)Math.Cos(angle); m6 = 0;
            m7 = 0;                         m8 = 0;                      m9 = 1;
        }
    }
}