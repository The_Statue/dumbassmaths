﻿using System;

namespace MathClasses
{
    public class Colour
    {
        public UInt32 colour;

        public Colour()
        {
            colour = 0;
        }
        public Colour(Byte r, Byte g, Byte b, Byte a)
        {
            colour = 0;
            colour += (UInt32)r << 24;
            colour += (UInt32)g << 16;
            colour += (UInt32)b << 8;
            colour += (UInt32)a;
        }
        public Byte GetRed()
        {
            UInt32 result = colour & 0xFF000000;
            result = (colour >> 24);
            return (Byte)result;
        }
        public Byte GetGreen()
        {
            UInt32 result = colour & 0x00FF0000;
            result = (colour >> 16);
            return (Byte)result;
        }
        public Byte GetBlue()
        {
            UInt32 result = colour & 0x0000FF00;
            result = (colour >> 8);
            return (Byte)result;
        }
        public Byte GetAlpha()
        {
            UInt32 result = colour & 0x000000FF;
            //result = (colour >> 0);
            return (Byte)result;
        }

        public void SetRed(byte input)
        {
            UInt32 result = (UInt32)(input << 24);
            colour = colour | result;
        }
        public void SetGreen(byte input)
        {
            UInt32 result = (UInt32)(input << 16);
            colour = colour | result;
        }
        public void SetBlue(byte input)
        {
            UInt32 result = (UInt32)(input << 8);
            colour = colour | result;
        }
        public void SetAlpha(byte input)
        {
            colour = colour | input;
        }
    }
}
