﻿using System;
namespace MathClasses
{
    public class Vector4
    {
        public float x, y, z, w;

        public Vector4()
        {
            x = 0;
            y = 0;
            z = 0;
            w = 0;
        }
        public Vector4(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        public static Vector4 operator +(Vector4 one, Vector4 two)
        {
            Vector4 sum = new Vector4();

            sum.x = one.x + two.x;
            sum.y = one.y + two.y;
            sum.z = one.z + two.z;
            sum.w = one.w + two.w;

            return sum;
        }

        public static Vector4 operator -(Vector4 one, Vector4 two)
        {
            Vector4 sum = new Vector4();

            sum.x = one.x - two.x;
            sum.y = one.y - two.y;
            sum.z = one.z - two.z;
            sum.w = one.w - two.w;

            return sum;
        }

        public static Vector4 operator *(Vector4 one, float two)
        {
            Vector4 sum = new Vector4();

            sum.x = one.x * two;
            sum.y = one.y * two;
            sum.z = one.z * two;
            sum.w = one.w * two;

            return sum;
        }

        public static Vector4 operator *(float one, Vector4 two)
        {
            Vector4 sum = new Vector4();

            sum.x = one * two.x;
            sum.y = one * two.y;
            sum.z = one * two.z;
            sum.w = one * two.w;

            return sum;
        }

        public static Vector4 operator *(Matrix4 one, Vector4 two)
        {
            Vector4 sum = new Vector4();

            sum.x = (one.m1 * two.x) + (one.m5 * two.y) + (one.m9 * two.z) + (one.m13 * two.w);
            sum.y = (one.m2 * two.x) + (one.m6 * two.y) + (one.m10 * two.z) + (one.m14 * two.w);
            sum.z = (one.m3 * two.x) + (one.m7 * two.y) + (one.m11 * two.z) + (one.m15 * two.w);
            sum.w = (one.m4 * two.x) + (one.m8 * two.y) + (one.m12 * two.z) + (one.m16 * two.w);

            return sum;
        }
        public float Dot(Vector4 two)
        {
            float sum = 0;

            sum += x * two.x;
            sum += y * two.y;
            sum += z * two.z;
            sum += w * two.w;

            return sum;
        }

        public Vector4 Cross(Vector4 two)
        {
            Vector4 sum = new Vector4();

            sum.z = (x * two.y) - (y * two.x);
            sum.x = (y * two.z) - (z * two.y);
            sum.y = (z * two.x) - (x * two.z);
            //sum.w = (w * two.x) - (x * two.w);

            return sum;
        }
        public float Magnitude()
        {
            float sum = (float)Math.Sqrt(x * x + y * y + z * z + w * w);

            return sum;
        }

        public float Normalize()
        {
            float sum = (float)Math.Sqrt(x * x + y * y + z * z + w * w);

            x /= sum;
            y /= sum;
            z /= sum;
            w /= sum;

            return sum;
        }
        //
        // NOTE:
        // These implicit operators will convert between a MathClasses.Vector4 & System.Numerics.Vector4
        // This is useful because Raylib has functions that work nicely with System.Numerics.Vector4, 
        // so we can pass our MathClasses.Vector4 directly to Raylib & let the implicit conversions do their job.         
        public static implicit operator Vector4(System.Numerics.Vector4 v)
        {
            return new Vector4 { x = v.X, y = v.Y, z = v.Z, w = v.W };
        }

        public static implicit operator System.Numerics.Vector4(Vector4 v)
        {
            return new System.Numerics.Vector4(v.x, v.y, v.z, v.w);
        }

    }
}
