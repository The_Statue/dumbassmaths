﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raylib;
using static Raylib.Raylib;
using static Raylib.Color;

namespace Project2D
{
    public class PhysicsObj
    {
        Matrix transform = new Matrix();
        public Vector2 position = new Vector2(0, 0);
        public Vector2 velocity = new Vector2(0, 0);
        public Vector2 force = new Vector2(0, 0);
        public Vector2 parentOffset = new Vector2(0, 0f);
        public float rotation = 0f;
        public Color color = WHITE;
        float mass = 0.1f;
        PhysicsObj parent;
        Image image;
        Texture2D hud = LoadTextureFromImage(ImageText("NotPhysics", 5, PURPLE));
        Texture2D texture;
        public PhysicsObj(Vector2 pos, Texture2D texture)
        {
            position = pos;
            this.texture = texture;
        }
        ~PhysicsObj()
        {
        }
        public void update()
        {
            position += velocity * GetFrameTime(); // apply velocity to position, decreased for frame speed
            velocity += force/mass * GetFrameTime();

            //if (velocity.y > 0)
            //    velocity.y--;
            //if (velocity.y < 0)
            //    velocity.y++;


            // apply drag for x and z axis
            if (velocity.y > 0)
                velocity.y /= 1.2f;
            if (velocity.y < 0)
                velocity.y /= 1.2f;
            if (velocity.x > 0)
                velocity.x /= 1.2f;
            if (velocity.x < 0)
                velocity.x /= 1.2f;

            //if (position.y - (size.y / 2) <= -16 || position.y - (size.y / 2) >= 16) // colliding with z axis walls
            //    velocity.y *= -1;
            //if (position.x - (size.x / 2) <= -16 || position.x - (size.x / 2) >= 16) // colliding with x axis walls
            //    velocity.x *= -1;
        }
        public void draw()
        {
            Vector2 tempPos = position;
            if (parent != null)
                tempPos += parent.position + parentOffset;
            DrawTextureEx(texture, position, rotation, 1, WHITE);
            //DrawCubeWiresV(tempPos, size, MAROON);
        }
        public void addParent(PhysicsObj parent)
        {
            this.parent = parent;
        }
    }
}
