﻿using Raylib;
using System;
using System.Numerics;
using System.Diagnostics;
using static Raylib.Raylib;
using static Raylib.Color;
using static Raylib.CameraType;
using static Raylib.CameraMode;
using static Raylib.KeyboardKey;
using static Raylib.MouseButton;
using static Raylib.MaterialMapType;
using System.Collections.Generic;
using MathClasses;
using Vector3 = Raylib.Vector3;
using Vector2 = Raylib.Vector2;

namespace Project2D
{
    public class Program
    {
        public static List<PhysicsObj> gameObjects = new List<PhysicsObj>();
        public static List<PhysicsObj> Selected = new List<PhysicsObj>();
        static bool destroyed = false;
        static bool paused = false;
        static void Main(string[] args)
        {
            // Initialization
            //--------------------------------------------------------------------------------------
            const int screenWidth = 1600;
            const int screenHeight = 900;
            const int boardSize = 2;
            const int speed = 500;
            int m = 0;

            MathClasses.Vector2 input = new MathClasses.Vector2(0, 0);

            Ray ray = new Ray();
            bool collision = false;

            InitWindow(screenWidth, screenHeight, "Something or rather");

            // Define the camera to look into our 3d world
            Camera2D camera = new Camera2D();
            camera.target = new Vector2(0, 0);
            camera.offset = new Vector2(screenWidth / 2, screenHeight / 2);
            camera.rotation = 0.0f;
            camera.zoom = 1.0f;

            //Model model = LoadModelFromMesh(GenMeshPlane(2, 2, 5, 5));
            //model = LoadModel("../images/Tank Body.obj"); // Model variable broken, unable to load models or generate meshes
            
            Texture2D tankBody = LoadTexture("../images/Tanks/tankGreen.png");

            //Add gameobjects
            gameObjects.Add(new PhysicsObj(new Vector2(0,0), tankBody));
            gameObjects.Add(new PhysicsObj(new Vector2(0, 0), tankBody));
            gameObjects[1].addParent(gameObjects[0]);


            SetTargetFPS(60);                   // Set our game to run at 60 frames-per-second
            //--------------------------------------------------------------------------------------

            // Main game loop
            while (!WindowShouldClose()) // Detect window close button or ESC key
            {
                // Update
                //----------------------------------------------------------------------------------

                //Camera position
                camera.target = gameObjects[0].position;

                if (IsKeyDown(KEY_SPACE)) gameObjects[0].velocity.y -= 25;
                input.y = 0;
                input.x = 0;
                if (IsKeyDown(KEY_W)) input.y--;
                if (IsKeyDown(KEY_S)) input.y++;
                if (IsKeyDown(KEY_A)) gameObjects[0].rotation--;
                if (IsKeyDown(KEY_D)) gameObjects[0].rotation++;

                input.Normalise();
                gameObjects[0].force.x = speed * input.x;
                gameObjects[0].force.y = speed * input.y;
                //paused = !paused;

                //if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
                //{
                //    ray = GetMouseRay(GetMousePosition(), camera);
                //    bool result = false;
                //    foreach (PhysicsObj gameObject in gameObjects)
                //    {
                //        BoundingBox box = new BoundingBox();
                //        box.min = new Vector3(gameObject.position.x - (gameObject.size.x / 2), gameObject.position.y - (gameObject.size.y / 2), gameObject.position.z - (gameObject.size.z / 2));
                //        box.max = new Vector3(gameObject.position.x + (gameObject.size.x / 2), gameObject.position.y + (gameObject.size.y / 2), gameObject.position.z + (gameObject.size.z / 2));
                //        collision = CheckCollisionRayBox(ray, box);
                //        if (collision)
                //        {
                //            result = true;
                //            if (Selected.Contains(gameObject))
                //                Selected.Remove(gameObject);
                //            Selected.Add(gameObject);
                //            gameObject.color = BLUE;
                //        }
                //    }
                //    foreach (PhysicsObj gameObject in gameObjects)
                //    {
                //        if (!Selected.Contains(gameObject) || !result)
                //        {
                //            gameObject.color = gameObject.Originalcolor;
                //            Selected.Remove(gameObject);
                //        }
                //    }
                //}

                if (!paused)
                    foreach (PhysicsObj gameObject in gameObjects)
                        gameObject.update();
                //----------------------------------------------------------------------------------

                // Draw
                //----------------------------------------------------------------------------------
                BeginDrawing();
                ClearBackground(RAYWHITE);

                BeginMode2D(camera);

                DrawRectangle(0, 0, 1000, 8000, DARKGRAY);

                foreach (PhysicsObj gameObject in gameObjects)
                    gameObject.draw();


                EndMode2D();

                DrawFPS(10, 10);
                DrawText(gameObjects[0].position.ToString(), 20, 50, 10, RED);
                DrawText(gameObjects[0].force.ToString(), 20, 70, 10, RED);

                EndDrawing();
                //----------------------------------------------------------------------------------
            }

            // De-Initialization
            //--------------------------------------------------------------------------------------
            CloseWindow();        // Close window and OpenGL context
            //--------------------------------------------------------------------------------------
        }
    }
}
